﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppCRM.Models
{
    public class Direccion
    {
        [Key]
        public int DireccionId { get; set; }

        public string Calle { get; set; }

        public string NumExterior { get; set; }

        public string NumInterior { get; set; }

        public string Colonia { get; set; }

        public string Municipio { get; set; }

        public string Estado { get; set; }

        public bool Principal { get; set; }

        //public virtual Cliente Cliente { get; set; }
    }
}