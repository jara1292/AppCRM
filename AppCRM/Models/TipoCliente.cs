﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppCRM.Models
{
    public class TipoCliente
    {
        [Key]
        public int TipoClienteId { get; set; }

        public string NombreTipo { get; set; }
    }
}