﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppCRM.Models
{
    public class Telefono
    {
        [Key]
        public int TelefonoId { get; set; }

        public string NumeroTelefonico { get; set; }

        public string Tipo { get; set; }

        public bool Principal { get; set; }

        //public virtual Cliente Cliente { get; set; }
    }
}