﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppCRM.Models
{
    public class Email
    {
        [Key]
        public int EmailId { get; set; }

        public string Direccion { get; set; }

        public bool Principal { get; set; }

        //public virtual Cliente Cliente { get; set; }
    }
}