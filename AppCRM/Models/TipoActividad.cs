﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppCRM.Models
{
    public class TipoActividad
    {
        [Key]
        public int TipoActividadId { get; set; }

        public string Descripcion { get; set; }
    }
}