﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppCRM.Models
{
    public class Campania
    {
        [Key]
        public int CampaniaId { get; set; }

        public string Nombre { get; set; }

        public DateTime FechaPlan { get; set; }

        public DateTime Fecha { get; set; }

        public ICollection<Actividad> Actividades { get; set; }
    }
}