﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppCRM.Models
{
    public class Actividad
    {
        [Key]
        public int ActividadId { get; set; }

        public string Descripcion { get; set; }

        public DateTime FechaInicial { get; set; }

        public DateTime FechaFinal { get; set; }

        public DateTime FechaInicialPlan { get; set; }

        public DateTime FechaFinalPlan { get; set; }

        public int Estado { get; set; }

        public int TipoActividadId { get; set; }

        public TipoActividad Tipo { get; set; }

        public int ClienteId { get; set; }

        public Cliente ClienteActividad { get; set; }

        public Campania CampaniaAct { get; set; }
    }
}