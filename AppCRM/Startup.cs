﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AppCRM.Startup))]
namespace AppCRM
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
